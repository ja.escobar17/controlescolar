use controlescolar;
-------------------
--Insertar maestro
CREATE PROCEDURE SP_InsertarMaestro @idMaestro int,@nombre varchar(100),@apellidos varchar(100), @telefono varchar(30),
@email varchar(80)
AS
BEGIN
	insert into dbo.maestros (idMaestro,nombre,apellidos,telefono,email) values (@idMaestro,@nombre,@apellidos,@telefono,@email)
END
GO
-------------------
--Actualizar maestro
CREATE PROCEDURE SP_ActualizarMaestro @idMaestro int,@nombre varchar(100),@apellidos varchar(100), @telefono varchar(30),
@email varchar(80)
AS
BEGIN
	update dbo.maestros set nombre = @nombre,apellidos = @apellidos,telefono = @telefono,email = @email where idMaestro = @idMaestro
END
GO
-------------------
--Eliminar maestro
CREATE PROCEDURE SP_EliminarMaestro @idMaestro int
AS
BEGIN
	delete from dbo.maestros where idMaestro = @idMaestro
END
GO
-------------------
select * from maestros
exec SP_InsertarMaestro 1,'Gerardo','Beltr�n Beltr�n','6672390158','g.beltran@escuela.com.mx'
