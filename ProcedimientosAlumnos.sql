use controlescolar;
drop procedure SP_InsertarAlumno
----------------------------------
--Insertar alumno
CREATE PROCEDURE SP_InsertarAlumno @matricula varchar(50),@nombre varchar(70),@apellidos varchar(70), @telefono varchar(30),
@email varchar(30),@grupo varchar(10)
AS
BEGIN
	insert into dbo.alumnos (matricula,nombre,apellidos,telefono,email,grupo) values (@matricula,@nombre,@apellidos,@telefono,@email,@grupo)
END
GO
-----------------------------------
--Actualizar alumno
CREATE PROCEDURE SP_ActualizarAlumno @matricula varchar(50),@nombre varchar(70),@apellidos varchar(70), @telefono varchar(30),
@email varchar(30),@grupo varchar(10)
AS
BEGIN
	update dbo.alumnos set nombre = @nombre,apellidos = @apellidos,telefono = @telefono,email = @email,grupo = @grupo where matricula = @matricula
END
GO
-----------------------------------
--Eliminar alumno
CREATE PROCEDURE SP_EliminarAlumno @matricula varchar(50)
AS
BEGIN
	delete from dbo.alumnos where matricula = @matricula
END
GO
-----------------------------------


use controlescolar;
exec SP_InsertarAlumno '21432575101354','Ana Paola','Olivas Sandoval','6673489310','anapa@gmail.com','4-2'
exec SP_ActualizarAlumno '17213805192356','Jes�s Antonio','Escobar Reyes','6671378269','jesusescobar99@hotmail.com','5-01'
exec SP_EliminarAlumno '21432575101354'

truncate table alumnos
select * from alumnos
