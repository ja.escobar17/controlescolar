using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace ControlEscolar.Formularios
{
    public partial class Materias : Form
    {
        //Inicializar variables
        string server = "Data Source = PC-TOSHIBA\\SQLEXPRESS; database=controlescolar; Integrated Security = True";
        SqlConnection conectar = new SqlConnection();
        Configuracion con = new Configuracion();
        DialogResult dialogResult;

        public Materias()
        {
            InitializeComponent();
        }

        //Instrucciones a ejecutar al cargar página
        private void Materias_Load(object sender, EventArgs e)
        {
            con.mostrar("dbo.materias", dgvMaterias);
            conectar.ConnectionString = server;
            SqlCommand comando = new SqlCommand("select idMaestro from maestros",conectar);
            conectar.Open();
            SqlDataReader registro = comando.ExecuteReader();
            while (registro.Read())
            {
                cbIDMaestro.Items.Add(registro["idMaestro"].ToString());
            }
            conectar.Close();
        }

        private void txtIDMateria_TextChanged(object sender, EventArgs e)
        {

        }

        //Botones programados
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            conectar.ConnectionString = server;
            conectar.Open();
            SqlCommand cmd = new SqlCommand("SP_InsertarMateria", conectar);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idMateria", txtIDMateria.Text);
            cmd.Parameters.AddWithValue("@nombre", txtNombre.Text);
            cmd.Parameters.AddWithValue("@idMaestroImparte", cbIDMaestro.Text);
            cmd.Parameters.AddWithValue("@limiteAlumnos", nudLimiteAlumnos.Value);

            try
            {
                cmd.ExecuteNonQuery();
                MessageBox.Show("Datos guardados");
            }
            catch (SqlException EX)
            {
                MessageBox.Show(EX.ToString());
                throw;
            }
            Limpiar();
            con.mostrar("dbo.materias", dgvMaterias);
            conectar.Close();
        }
        private void btnModificar_Click(object sender, EventArgs e)
        {
            conectar.ConnectionString = server;
            conectar.Open();
            SqlCommand cmd = new SqlCommand("SP_ActualizarMateria", conectar);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idMateria", txtIDMateria.Text);
            cmd.Parameters.AddWithValue("@nombre", txtNombre.Text);
            cmd.Parameters.AddWithValue("@idMaestroImparte", cbIDMaestro.Text);
            cmd.Parameters.AddWithValue("@limiteAlumnos", nudLimiteAlumnos.Value);

            try
            {
                cmd.ExecuteNonQuery();
                MessageBox.Show("Datos actualizados");
            }
            catch (SqlException EX)
            {
                MessageBox.Show(EX.ToString());
                throw;
            }
            Limpiar();
            con.mostrar("dbo.materias", dgvMaterias);
            conectar.Close();
        }
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            dialogResult = MessageBox.Show("¿Está seguro de eliminar materia?", "Verificación de Eliminación", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dialogResult == DialogResult.Yes)
            {
                conectar.ConnectionString = server;
                conectar.Open();
                SqlCommand cmd = new SqlCommand("SP_EliminarMateria", conectar);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@idMateria", txtIDMateria.Text);

                try
                {
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Se ha eliminado materia");
                }
                catch (SqlException EX)
                {
                    MessageBox.Show(EX.ToString());
                    throw;
                }
                Limpiar();
                con.mostrar("dbo.materias", dgvMaterias);
                conectar.Close();
            }
        }
        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            Limpiar();
        }
        //Protección a campos de DataGridView
        private void dgvMaterias_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                txtIDMateria.Text = dgvMaterias.SelectedCells[0].Value.ToString();
                txtNombre.Text = dgvMaterias.SelectedCells[1].Value.ToString();
                cbIDMaestro.Text = dgvMaterias.SelectedCells[2].Value.ToString();
                nudLimiteAlumnos.Value = Convert.ToInt32(dgvMaterias.SelectedCells[3].Value.ToString());
            }
            catch (Exception)
            {

            }
        }

        //Método para limpiar textbox y combobox
        public void Limpiar()
        {
            txtIDMateria.Text = string.Empty;
            txtNombre.Text = string.Empty;
            cbIDMaestro.Text = string.Empty;
            nudLimiteAlumnos.Value = 0;
        }

        //Limitar tipos de datos
        private void txtIDMateria_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) || char.IsSymbol(e.KeyChar) || char.IsWhiteSpace(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = true;
            }
        }
        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}
