using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace ControlEscolar.Formularios
{
    public partial class Alumnos : Form
    {
        //Declarar variables
        string server = "Data Source = PC-TOSHIBA\\SQLEXPRESS; database=controlescolar; Integrated Security = True";
        SqlConnection conectar = new SqlConnection();
        Configuracion con = new Configuracion();
        SqlDataAdapter tabla;
        string Opcion;
        DialogResult dialogResult;
        

        //Inicializar variables
        public Alumnos()
        {
            InitializeComponent();
            this.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            Opcion = "apellidos";
        }

        //Instrucciones a ejecutar al cargar página
        private void Alumnos_Load(object sender, EventArgs e)
        {
            con.mostrar("dbo.alumnos", dgViewAlumnos);
        }
        //Botones programados
        private void buttonGuardar_Click(object sender, EventArgs e)
        {
            if (this.ValidateChildren(ValidationConstraints.Enabled))
            {
                conectar.ConnectionString = server;
                conectar.Open();
                SqlCommand cmd = new SqlCommand("SP_InsertarAlumno", conectar);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@matricula", txtMatricula.Text);
                cmd.Parameters.AddWithValue("@nombre", txtNombre.Text);
                cmd.Parameters.AddWithValue("@apellidos", txtApellidos.Text);
                cmd.Parameters.AddWithValue("@telefono", txtTelefono.Text);
                cmd.Parameters.AddWithValue("@email", txtEmail.Text);
                cmd.Parameters.AddWithValue("@grupo", cbGrupo.Text);

                try
                {
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Datos guardados");
                }
                catch (SqlException EX)
                {
                    MessageBox.Show(EX.ToString());
                    throw;
                }
                Limpiar();
                con.mostrar("dbo.alumnos", dgViewAlumnos);
                conectar.Close();
            }
            else
            {
                MessageBox.Show("Llene todos los campos");
            }            
        }        
        private void btnModificar_Click(object sender, EventArgs e)
        {
            conectar.ConnectionString = server;
            conectar.Open();
            SqlCommand cmd = new SqlCommand("SP_ActualizarAlumno", conectar);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@matricula", txtMatricula.Text);
            cmd.Parameters.AddWithValue("@nombre", txtNombre.Text);
            cmd.Parameters.AddWithValue("@apellidos", txtApellidos.Text);
            cmd.Parameters.AddWithValue("@telefono", txtTelefono.Text);
            cmd.Parameters.AddWithValue("@email", txtEmail.Text);
            cmd.Parameters.AddWithValue("@grupo", cbGrupo.Text);

            try
            {
                cmd.ExecuteNonQuery();
                MessageBox.Show("Datos actualizados");
            }
            catch (SqlException EX)
            {
                MessageBox.Show(EX.ToString());
                throw;
            }
            Limpiar();
            con.mostrar("dbo.alumnos", dgViewAlumnos);
            conectar.Close();
        }
        private void buttonLimpiar_Click(object sender, EventArgs e)
        {
            Limpiar();
        }
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            dialogResult = MessageBox.Show("¿Está seguro de eliminar alumno?", "Verificación de Eliminación", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dialogResult == DialogResult.Yes)
            {
                conectar.ConnectionString = server;
                conectar.Open();
                SqlCommand cmd = new SqlCommand("SP_EliminarAlumno", conectar);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@matricula", txtMatricula.Text);

                try
                {
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Se ha eliminado alumno");
                }
                catch (SqlException EX)
                {
                    MessageBox.Show(EX.ToString());
                    throw;
                }
                Limpiar();
                con.mostrar("dbo.alumnos", dgViewAlumnos);
                conectar.Close();
            }                
        }
        //Protección a campos de DataGridView
        private void dgViewAlumnos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                txtMatricula.Text = dgViewAlumnos.SelectedCells[0].Value.ToString();
                txtNombre.Text = dgViewAlumnos.SelectedCells[1].Value.ToString();
                txtApellidos.Text = dgViewAlumnos.SelectedCells[2].Value.ToString();
                txtTelefono.Text = dgViewAlumnos.SelectedCells[3].Value.ToString();
                txtEmail.Text = dgViewAlumnos.SelectedCells[4].Value.ToString();
                cbGrupo.SelectedItem = dgViewAlumnos.SelectedCells[5].Value.ToString();
            }
            catch (Exception)
            {

            }
        }
        //Limitar tipos de datos
        private void textBoxTelefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) || char.IsSymbol(e.KeyChar) || char.IsWhiteSpace(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = true;
            }
        }
        private void textBoxMatricula_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) || char.IsSymbol(e.KeyChar) || char.IsWhiteSpace(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = true;
            }
        }
        private void textBoxNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
        private void textBoxApellidos_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
        //Método para validar el email ingresado
        public static bool validarEmail(string email)
        {
            string expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if (Regex.IsMatch(email,expresion))
            {
                if (Regex.Replace(email, expresion, string.Empty).Length == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        private void textBoxEmail_Leave(object sender, EventArgs e)
        {
            if (validarEmail(txtEmail.Text))
            {

            }
            else
            {
                MessageBox.Show("Dirección de correcto eléctronico inválido");
                txtEmail.SelectAll();
                txtEmail.Focus();
            }
        }
        //Método para limpiar textbox y combobox
        public void Limpiar()
        {
            txtMatricula.Text = string.Empty;
            txtNombre.Text = string.Empty;
            txtApellidos.Text = string.Empty;
            txtTelefono.Text = string.Empty;
            txtEmail.Text = string.Empty;
            cbGrupo.SelectedItem = string.Empty;
        }
        //Metodo para filtrar alumnos por apellido
        public void BusquedaTiempoReal(string Resultado)
        {
            conectar.ConnectionString = server;
            conectar.Open();
            string Busqueda = string.Empty;
            Busqueda = "select * from dbo.alumnos where " + Opcion + " LIKE '%" + Resultado + "%';";
            tabla = new SqlDataAdapter(Busqueda, conectar);
            DataTable table = new DataTable();
            tabla.Fill(table);
            dgViewAlumnos.DataSource = table;
            conectar.Close();
        }
        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            BusquedaTiempoReal(txtFiltro.Text);
        }
    }
}
