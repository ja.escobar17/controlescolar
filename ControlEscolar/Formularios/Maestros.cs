using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace ControlEscolar.Formularios
{
    public partial class Maestros : Form
    {
        //Declarar variables
        string server = "Data Source = PC-TOSHIBA\\SQLEXPRESS; database=controlescolar; Integrated Security = True";
        SqlConnection conectar = new SqlConnection();
        Configuracion con = new Configuracion();
        SqlDataAdapter tabla;
        string Opcion;
        DialogResult dialogResult;

        //Inicializar variables
        public Maestros()
        {
            InitializeComponent();
            Opcion = "apellidos";
        }

        //Instrucciones a ejecutar al cargar página
        private void Maestros_Load(object sender, EventArgs e)
        {
            con.mostrar("dbo.maestros", dgvMaestros);
        }

        //Botones programados
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            conectar.ConnectionString = server;
            conectar.Open();
            SqlCommand cmd = new SqlCommand("SP_InsertarMaestro", conectar);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idMaestro", txtIDMaestro.Text);
            cmd.Parameters.AddWithValue("@nombre", txtNombre.Text);
            cmd.Parameters.AddWithValue("@apellidos", txtApellidos.Text);
            cmd.Parameters.AddWithValue("@telefono", txtTelefono.Text);
            cmd.Parameters.AddWithValue("@email", txtEmail.Text);

            try
            {
                cmd.ExecuteNonQuery();
                MessageBox.Show("Datos guardados");
            }
            catch (SqlException EX)
            {
                MessageBox.Show(EX.ToString());
                throw;
            }
            Limpiar();
            con.mostrar("dbo.maestros", dgvMaestros);
            conectar.Close();
        }
        private void btnModificar_Click(object sender, EventArgs e)
        {
            conectar.ConnectionString = server;
            conectar.Open();
            SqlCommand cmd = new SqlCommand("SP_ActualizarMaestro", conectar);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idMaestro", txtIDMaestro.Text);
            cmd.Parameters.AddWithValue("@nombre", txtNombre.Text);
            cmd.Parameters.AddWithValue("@apellidos", txtApellidos.Text);
            cmd.Parameters.AddWithValue("@telefono", txtTelefono.Text);
            cmd.Parameters.AddWithValue("@email", txtEmail.Text);

            try
            {
                cmd.ExecuteNonQuery();
                MessageBox.Show("Datos actualizados");
            }
            catch (SqlException EX)
            {
                MessageBox.Show(EX.ToString());
                throw;
            }
            Limpiar();
            con.mostrar("dbo.maestros", dgvMaestros);
            conectar.Close();
        }
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            dialogResult = MessageBox.Show("¿Está seguro de eliminar maestro?", "Verificación de Eliminación", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dialogResult == DialogResult.Yes)
            {
                conectar.ConnectionString = server;
                conectar.Open();
                SqlCommand cmd = new SqlCommand("SP_EliminarMaestro", conectar);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@idMaestro", txtIDMaestro.Text);

                try
                {
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Se ha eliminado maestro");
                }
                catch (SqlException EX)
                {
                    MessageBox.Show(EX.ToString());
                    throw;
                }
                Limpiar();
                con.mostrar("dbo.maestros", dgvMaestros);
                conectar.Close();
            }
        }
        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            Limpiar();
        }

        //Protección a campos de DataGridView
        private void dgvMaestros_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                txtIDMaestro.Text = dgvMaestros.SelectedCells[0].Value.ToString();
                txtNombre.Text = dgvMaestros.SelectedCells[1].Value.ToString();
                txtApellidos.Text = dgvMaestros.SelectedCells[2].Value.ToString();
                txtTelefono.Text = dgvMaestros.SelectedCells[3].Value.ToString();
                txtEmail.Text = dgvMaestros.SelectedCells[4].Value.ToString();
            }
            catch (Exception)
            {

            }
        }

        //Limitar tipos de datos
        private void txtTelefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) || char.IsSymbol(e.KeyChar) || char.IsWhiteSpace(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = true;
            }
        }
        private void txtIDMaestro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) || char.IsSymbol(e.KeyChar) || char.IsWhiteSpace(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = true;
            }
        }
        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
        private void txtApellidos_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        //Método para limpiar textbox y combobox
        public void Limpiar()
        {
            txtIDMaestro.Text = string.Empty;
            txtNombre.Text = string.Empty;
            txtApellidos.Text = string.Empty;
            txtTelefono.Text = string.Empty;
            txtEmail.Text = string.Empty;
        }

        //Método para validar el email ingresado
        public static bool validarEmail(string email)
        {
            string expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if (Regex.IsMatch(email, expresion))
            {
                if (Regex.Replace(email, expresion, string.Empty).Length == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        private void txtEmail_Leave(object sender, EventArgs e)
        {
            if (validarEmail(txtEmail.Text))
            {

            }
            else
            {
                MessageBox.Show("Dirección de correcto eléctronico inválido");
                txtEmail.SelectAll();
                txtEmail.Focus();
            }
        }

        //Metodo para filtrar alumnos por apellido
        public void BusquedaTiempoReal(string Resultado)
        {
            conectar.ConnectionString = server;
            conectar.Open();
            string Busqueda = string.Empty;
            Busqueda = "select * from dbo.maestros where " + Opcion + " LIKE '%" + Resultado + "%';";
            tabla = new SqlDataAdapter(Busqueda, conectar);
            DataTable table = new DataTable();
            tabla.Fill(table);
            dgvMaestros.DataSource = table;
            conectar.Close();
        }
        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            BusquedaTiempoReal(txtFiltro.Text);
        }
    }
}
