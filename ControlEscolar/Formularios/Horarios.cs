using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace ControlEscolar.Formularios
{
    public partial class Horarios : Form
    {
        //Declarar variables
        string server = "Data Source = PC-TOSHIBA\\SQLEXPRESS; database=controlescolar; Integrated Security = True";
        SqlConnection conectar = new SqlConnection();
        Configuracion con = new Configuracion();
        DialogResult dialogResult;

        public Horarios()
        {
            InitializeComponent();
        }

        //Instrucciones a ejecutar al cargar página
        private void Horarios_Load(object sender, EventArgs e)
        {
            con.mostrar("dbo.horarios", dgvHorarios);
        }

        //Botones programados
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            conectar.ConnectionString = server;
            conectar.Open();
            SqlCommand cmd = new SqlCommand("SP_InsertarHorario", conectar);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@idHorario", txtIDHorario.Text);
            cmd.Parameters.AddWithValue("@hora_inicio","'" + nudHoraInicio.Value + ":" + nudMinutoInicio + "'");
            cmd.Parameters.AddWithValue("@hora_fin","'" + nudHoraFin.Value + ":" + nudMinutoFin.Value + "'");

            try
            {
                cmd.ExecuteNonQuery();
                MessageBox.Show("Datos guardados");
            }
            catch (SqlException EX)
            {
                MessageBox.Show(EX.ToString());
                throw;
            }
            Limpiar();
            con.mostrar("dbo.horarios", dgvHorarios);
            conectar.Close();
        }

        //Método para limpiar textbox y combobox
        public void Limpiar()
        {
            txtIDHorario.Text = string.Empty;
            nudHoraInicio.Value = 0;
            nudMinutoInicio.Value = 0;
            nudHoraFin.Value = 0;
            nudMinutoFin.Value = 0;
        }
    }
}
