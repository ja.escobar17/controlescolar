namespace ControlEscolar.Formularios
{
    partial class Horarios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label7 = new System.Windows.Forms.Label();
            this.txtIDHorario = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.dgvHorarios = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.nudHoraInicio = new System.Windows.Forms.NumericUpDown();
            this.nudMinutoInicio = new System.Windows.Forms.NumericUpDown();
            this.nudHoraFin = new System.Windows.Forms.NumericUpDown();
            this.nudMinutoFin = new System.Windows.Forms.NumericUpDown();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnModificar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHorarios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHoraInicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinutoInicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHoraFin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinutoFin)).BeginInit();
            this.SuspendLayout();
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(12, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(33, 20);
            this.label7.TabIndex = 31;
            this.label7.Text = "ID:";
            // 
            // txtIDHorario
            // 
            this.txtIDHorario.Location = new System.Drawing.Point(69, 9);
            this.txtIDHorario.Name = "txtIDHorario";
            this.txtIDHorario.Size = new System.Drawing.Size(223, 20);
            this.txtIDHorario.TabIndex = 37;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 20);
            this.label1.TabIndex = 38;
            this.label1.Text = "Hora de inicio:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(176, 20);
            this.label2.TabIndex = 39;
            this.label2.Text = "Hora de terminación:";
            // 
            // dgvHorarios
            // 
            this.dgvHorarios.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvHorarios.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvHorarios.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvHorarios.BackgroundColor = System.Drawing.SystemColors.InactiveBorder;
            this.dgvHorarios.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvHorarios.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dgvHorarios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHorarios.EnableHeadersVisualStyles = false;
            this.dgvHorarios.Location = new System.Drawing.Point(375, 46);
            this.dgvHorarios.Name = "dgvHorarios";
            this.dgvHorarios.RowHeadersWidth = 20;
            this.dgvHorarios.Size = new System.Drawing.Size(643, 248);
            this.dgvHorarios.TabIndex = 41;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(381, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(280, 20);
            this.label3.TabIndex = 54;
            this.label3.Text = "↓ (Selecciona para llenar campos)";
            // 
            // nudHoraInicio
            // 
            this.nudHoraInicio.Location = new System.Drawing.Point(194, 59);
            this.nudHoraInicio.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.nudHoraInicio.Name = "nudHoraInicio";
            this.nudHoraInicio.Size = new System.Drawing.Size(46, 20);
            this.nudHoraInicio.TabIndex = 55;
            // 
            // nudMinutoInicio
            // 
            this.nudMinutoInicio.Location = new System.Drawing.Point(246, 59);
            this.nudMinutoInicio.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.nudMinutoInicio.Name = "nudMinutoInicio";
            this.nudMinutoInicio.Size = new System.Drawing.Size(46, 20);
            this.nudMinutoInicio.TabIndex = 56;
            // 
            // nudHoraFin
            // 
            this.nudHoraFin.Location = new System.Drawing.Point(194, 107);
            this.nudHoraFin.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.nudHoraFin.Name = "nudHoraFin";
            this.nudHoraFin.Size = new System.Drawing.Size(46, 20);
            this.nudHoraFin.TabIndex = 57;
            // 
            // nudMinutoFin
            // 
            this.nudMinutoFin.Location = new System.Drawing.Point(246, 107);
            this.nudMinutoFin.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.nudMinutoFin.Name = "nudMinutoFin";
            this.nudMinutoFin.Size = new System.Drawing.Size(46, 20);
            this.nudMinutoFin.TabIndex = 58;
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLimpiar.Location = new System.Drawing.Point(12, 169);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(79, 38);
            this.btnLimpiar.TabIndex = 62;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            // 
            // btnEliminar
            // 
            this.btnEliminar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminar.Location = new System.Drawing.Point(97, 169);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(82, 38);
            this.btnEliminar.TabIndex = 61;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = true;
            // 
            // btnModificar
            // 
            this.btnModificar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModificar.Location = new System.Drawing.Point(185, 169);
            this.btnModificar.Name = "btnModificar";
            this.btnModificar.Size = new System.Drawing.Size(92, 38);
            this.btnModificar.TabIndex = 60;
            this.btnModificar.Text = "Modificar";
            this.btnModificar.UseVisualStyleBackColor = true;
            // 
            // btnGuardar
            // 
            this.btnGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.Location = new System.Drawing.Point(283, 169);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(83, 38);
            this.btnGuardar.TabIndex = 59;
            this.btnGuardar.Text = "Agregar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // Horarios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1034, 426);
            this.Controls.Add(this.btnLimpiar);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnModificar);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.nudMinutoFin);
            this.Controls.Add(this.nudHoraFin);
            this.Controls.Add(this.nudMinutoInicio);
            this.Controls.Add(this.nudHoraInicio);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dgvHorarios);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtIDHorario);
            this.Controls.Add(this.label7);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Horarios";
            this.Text = "Horarios";
            this.Load += new System.EventHandler(this.Horarios_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvHorarios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHoraInicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinutoInicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHoraFin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinutoFin)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtIDHorario;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.DataGridView dgvHorarios;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown nudHoraInicio;
        private System.Windows.Forms.NumericUpDown nudMinutoInicio;
        private System.Windows.Forms.NumericUpDown nudHoraFin;
        private System.Windows.Forms.NumericUpDown nudMinutoFin;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnModificar;
        private System.Windows.Forms.Button btnGuardar;
    }
}
