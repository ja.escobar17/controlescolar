using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using ControlEscolar.Formularios;

namespace ControlEscolar
{
    public partial class frmPrincipal : Form
    {        
        Alumnos alumnos;
        Maestros maestros;
        Materias materias;
        Horarios horarios;
                
        public frmPrincipal()
        {
            InitializeComponent();
            alumnos = new Alumnos();
            maestros = new Maestros();
            materias = new Materias();
            horarios = new Horarios();
        }
        

        private void btnAlumnos_Click(object sender, EventArgs e)
        {
            if (this.PanelContenedor.Controls.Count > 0)
                this.PanelContenedor.Controls.RemoveAt(0);
            alumnos.TopLevel = false;
            alumnos.Dock = DockStyle.Fill;
            this.PanelContenedor.Controls.Add(alumnos);
            this.PanelContenedor.Tag = alumnos;
            alumnos.Show();
            this.Text = "Control Escolar - Alumnos";
        }

        private void btnMaestros_Click(object sender, EventArgs e)
        {
            if (this.PanelContenedor.Controls.Count > 0)
                this.PanelContenedor.Controls.RemoveAt(0);
            maestros.TopLevel = false;
            maestros.Dock = DockStyle.Fill;
            this.PanelContenedor.Controls.Add(maestros);
            this.PanelContenedor.Tag = maestros;
            maestros.Show();
            this.Text = "Control Escolar - Maestros";
        }

        private void btnMaterias_Click(object sender, EventArgs e)
        {
            if (this.PanelContenedor.Controls.Count > 0)
                this.PanelContenedor.Controls.RemoveAt(0);
            materias.TopLevel = false;
            materias.Dock = DockStyle.Fill;
            this.PanelContenedor.Controls.Add(materias);
            this.PanelContenedor.Tag = materias;
            materias.Show();
            this.Text = "Control Escolar - Materias";
        }

        private void btnHorarios_Click(object sender, EventArgs e)
        {
            if (this.PanelContenedor.Controls.Count > 0)
                this.PanelContenedor.Controls.RemoveAt(0);
            horarios.TopLevel = false;
            horarios.Dock = DockStyle.Fill;
            this.PanelContenedor.Controls.Add(horarios);
            this.PanelContenedor.Tag = horarios;
            horarios.Show();
            this.Text = "Control Escolar - Horarios";
        }
    }
}
