USE [controlescolar]
GO

/****** Object:  Table [dbo].[alumnos]    Script Date: 07/12/2021 10:41:12 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[alumnos](
	[matricula] [varchar](50) NOT NULL,
	[nombre] [varchar](70) NOT NULL,
	[apellidos] [varchar](70) NOT NULL,
	[telefono] [varchar](30) NOT NULL,
	[email] [varchar](50) NOT NULL,
	[grupo] [varchar](10) NOT NULL,
 CONSTRAINT [PK_alumnos] PRIMARY KEY CLUSTERED 
(
	[matricula] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[alumnos_horarios]    Script Date: 07/12/2021 10:41:12 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[alumnos_horarios](
	[matricula] [varchar](50) NOT NULL,
	[idHorarioMateria] [int] NOT NULL,
 CONSTRAINT [PK_alumnos_horarios] PRIMARY KEY CLUSTERED 
(
	[matricula] ASC,
	[idHorarioMateria] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[calificaciones_alumnos]    Script Date: 07/12/2021 10:41:12 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[calificaciones_alumnos](
	[matricula] [varchar](50) NOT NULL,
	[idMateria] [int] NOT NULL,
	[calificacion] [smallmoney] NOT NULL,
 CONSTRAINT [PK_calificaciones_alumnos] PRIMARY KEY CLUSTERED 
(
	[matricula] ASC,
	[idMateria] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[horarios]    Script Date: 07/12/2021 10:41:12 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[horarios](
	[idHorario] [int] NOT NULL,
	[hora_inicio] [time](7) NULL,
	[hora_fin] [time](7) NULL,
 CONSTRAINT [PK_horarios] PRIMARY KEY CLUSTERED 
(
	[idHorario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[horarios_materias]    Script Date: 07/12/2021 10:41:12 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[horarios_materias](
	[idHorarioMateria] [int] NOT NULL,
	[idHorario] [int] NOT NULL,
	[idMateria] [int] NOT NULL,
 CONSTRAINT [PK_horarios_materias] PRIMARY KEY CLUSTERED 
(
	[idHorarioMateria] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[maestros]    Script Date: 07/12/2021 10:41:12 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[maestros](
	[idMaestro] [int] NOT NULL,
	[nombre] [varchar](100) NOT NULL,
	[apellidos] [varchar](100) NOT NULL,
	[telefono] [varchar](30) NULL,
	[email] [varchar](80) NULL,
 CONSTRAINT [PK_maestros] PRIMARY KEY CLUSTERED 
(
	[idMaestro] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[materias]    Script Date: 07/12/2021 10:41:12 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[materias](
	[idMateria] [int] NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[idMaestroImparte] [int] NOT NULL,
	[limiteAlumnos] [int] NOT NULL,
 CONSTRAINT [PK_materias] PRIMARY KEY CLUSTERED 
(
	[idMateria] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[materias_alumnos]    Script Date: 07/12/2021 10:41:12 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[materias_alumnos](
	[matricula] [varchar](50) NOT NULL,
	[idMateria] [int] NOT NULL,
 CONSTRAINT [PK_materias_alumnos] PRIMARY KEY CLUSTERED 
(
	[matricula] ASC,
	[idMateria] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[materias_asignadas](
	[idMateria] [int] NOT NULL,
	[idMaestro] [int] NOT NULL,
 CONSTRAINT [PK_materias_asignadas] PRIMARY KEY CLUSTERED 
(
	[idMateria] ASC,
	[idMaestro] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


