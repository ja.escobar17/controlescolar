use controlescolar

----------------------------------
--Insertar materia
CREATE PROCEDURE SP_InsertarMateria @idMateria int,@nombre varchar(50),@idMaestroImparte int, @limiteAlumnos int
AS
BEGIN
	insert into dbo.materias (idMateria,nombre,idMaestroImparte,limiteAlumnos) values (@idMateria,@nombre,@idMaestroImparte,@limiteAlumnos)
END
GO
-----------------------------------
--Actualizar materia
CREATE PROCEDURE SP_ActualizarMateria @idMateria int,@nombre varchar(50),@idMaestroImparte int, @limiteAlumnos int
AS
BEGIN
	update dbo.materias set nombre = @nombre,idMaestroImparte = @idMaestroImparte,limiteAlumnos = @limiteAlumnos  where idMateria = @idMateria
END
GO
-----------------------------------
--Eliminar materia
CREATE PROCEDURE SP_EliminarMateria @idMateria int
AS
BEGIN
	delete from dbo.materias where idMateria = @idMateria
END
GO
-------------------
exec SP_InsertarMateria 1,'ETICA',2,30
select materias.idMateria,materias.nombre as [Nombre Materia] from materias
SELECT * FROM maestros
