use controlescolar

----------------------------------
--Insertar horario
CREATE PROCEDURE SP_InsertarHorario @idHorario int,@hora_inicio time,@hora_fin time
AS
BEGIN
	insert into dbo.horarios (idHorario,hora_inicio,hora_fin) values (@idHorario,@hora_inicio,@hora_fin)
END
GO
-----------------------------------
exec SP_InsertarHorario 2,'8:0','9:0'

select * from horarios
